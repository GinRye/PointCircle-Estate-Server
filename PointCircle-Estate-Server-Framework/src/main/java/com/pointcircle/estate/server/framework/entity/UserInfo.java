package com.pointcircle.estate.server.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_user_info",
	uniqueConstraints = {
		@UniqueConstraint(name = "uq_user_info_user_id", columnNames = {"user_id"})
	}
)
public class UserInfo extends JpaBaseEntity<String> {

	@Id
	@Column(name = "user_info_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_user_info_user_id"))
	private User user;

	@Column(name = "user_id", updatable = false, insertable = false)
	private String userId;
	
	@Column(name = "mobilephone", nullable = false)
	private String mobilephone;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "effective_date")
	private Date effectiveDate;
	
	@Column(name = "is_lock")
	private boolean isLock = false;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_user_info_organization_id"))
	private Organization organization;
	
	@Column(name = "organization_id", updatable = false, insertable = false)
	private String organizationId;

	@Column(name = "app_permission")
	private String appPermission;

}
