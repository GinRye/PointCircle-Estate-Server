package com.pointcircle.estate.server.framework.web.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindByIdRest;
import com.pointcircle.core.web.RestResponseElement;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;
import com.pointcircle.estate.server.framework.entity.CheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.CheckQualityRepair;
import com.pointcircle.estate.server.framework.entity.Ids;
import com.pointcircle.estate.server.framework.repository.CheckQualityBatchRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityProblemRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityRepairRepository;
import com.pointcircle.estate.server.framework.web.api.CheckQualityProblemApi.CheckQualityProblemEntityVo;

@RestController
@RequestMapping("/api/checkQualityRepair")
public class CheckQualityRepairApi implements 
	IFindByIdRest<CheckQualityRepair, String, CheckQualityRepairApi.CheckQualityRepairEntityVo> {

	@Autowired
	private CheckQualityRepairRepository repository;

	@Autowired
	private com.pointcircle.core.repository.UserRepository userRepository;
	
	@Autowired
	private CheckQualityProblemRepository checkQualityProblemRepository;
	
	@Autowired
	private CheckQualityProblemApi v3CheckQualityProblemApi;
	
	@Autowired
	private CheckQualityBatchRepository checkQualityBatchRepository;
	public class CheckQualityRepairEntityVo extends EntityVo<CheckQualityRepair, String> {

		public CheckQualityRepairEntityVo(CheckQualityRepair entity) {
			super(entity);
		}
		@JsonProperty("checkqualityProblemId")
		public String getCheckqualityProblemId() {
			return entity.getCheckqualityProblemId();
		}
		@JsonProperty("repair")
		public String repair() {
			return entity.getRepairId();
		}
		@JsonProperty("parentCheckqualityRepairId")
		public String parentCheckqualityRepairId() {
			return entity.getParentCheckQualityRepairId();
		}
		@JsonProperty("childCheckqualityRepairId")
		public String childCheckqualityRepairId() {
			return entity.getChildCheckQualityRepairId();
		}
	}

	@Override
	public IRepository<CheckQualityRepair, String> getRepository() {
		return repository;
	}
	
	@GetMapping("findByBatchId")
	public RestResponseList findByBatchId(
			@RequestParam("batchId") String batchId){
		CheckQualityBatch batch = checkQualityBatchRepository.findOne(batchId);
		List<CheckQualityProblem> problems = checkQualityProblemRepository.findByCheckQualityBatch(batch);
		List<CheckQualityRepair> CheckQualityRepairs = problems.size() > 0 ? 
			repository.findByCheckQualityProblemIn(problems) : 
			new LinkedList<CheckQualityRepair>();
		return this.createRestResponseList(CheckQualityRepairs);
	}
	
	@GetMapping("findByProblemIds")
	public RestResponseList findByProblemIds(@RequestParam("problemIds") List<String> problemIds){
		if(problemIds.size() < 1) {
			return this.createRestResponseList(new LinkedList<CheckQualityRepair>());
		}
		List<CheckQualityProblem> problems = checkQualityProblemRepository.findProblemsByProblemIds(problemIds);
		List<CheckQualityRepair> CheckQualityRepairs = problems.size() > 0 ? 
			repository.findByCheckQualityProblemIn(problems) : 
			new LinkedList<CheckQualityRepair>();
		return this.createRestResponseList(CheckQualityRepairs);
	}
	
	@GetMapping("findByCheckQualityProblemId")
	public RestResponseList findByCheckQualityProblemId(
		@RequestParam("checkQualityProblemId") String CheckQualityProblemId) {
		CheckQualityProblem checkQualityProblem = checkQualityProblemRepository.findOne(CheckQualityProblemId);
		List<CheckQualityRepair> CheckQualityRepairs = repository.findByCheckQualityProblem(checkQualityProblem);
		return this.createRestResponseList(CheckQualityRepairs);
	}
	
	@GetMapping("changeRepair")
	public RestResponseElement changeRepair(
			@RequestParam(value = "checkQualityProblemId") String checkQualityProblemId,
			@RequestParam(value = "newRepairId") String newRepairId) {
		CheckQualityProblem checkQualityProblem = checkQualityProblemRepository.findOne(checkQualityProblemId);
		User newRepair = userRepository.findOne(newRepairId);
		List<CheckQualityRepair> repairList = repository.findByCheckQualityProblem(checkQualityProblem);
		boolean isHaveRepair = false;
		CheckQualityRepair tailRepair = null;
		for(CheckQualityRepair repair : repairList) {
			if(repair.getRepair() == newRepair) {
				isHaveRepair = true;
			}
			if(repair.getChildCheckQualityRepair() == null) {
				tailRepair = repair;
			}
		}
		RestResponseElement response = new RestResponseElement();
		if(!isHaveRepair) {
			CheckQualityRepair newCheckQualityRepair = new CheckQualityRepair();
			newCheckQualityRepair.setCheckQualityProblem(checkQualityProblem);
			newCheckQualityRepair.setParentCheckQualityRepair(tailRepair);
			newCheckQualityRepair.setRepair(newRepair);
			this.getRepository().saveAndFlush(newCheckQualityRepair);
			tailRepair.setChildCheckQualityRepair(newCheckQualityRepair);
			this.getRepository().saveAndFlush(tailRepair);
			checkQualityProblem.setUpdateTime(new Date());
			checkQualityProblemRepository.saveAndFlush(checkQualityProblem);
			response.setSuccess(true);
			Map<String, Object> map = new HashMap<String, Object>();
			CheckQualityProblemEntityVo problemVo = v3CheckQualityProblemApi.generateEntityVo(checkQualityProblem);
			List<CheckQualityRepair> repairs = repository.findByCheckQualityProblem(checkQualityProblem);
			List<CheckQualityRepairEntityVo> repairVoList = new ArrayList<>();
			for(CheckQualityRepair repair : repairs) {
				repairVoList.add(new CheckQualityRepairEntityVo(repair));
			}
			map.put("repairList", repairVoList);
			map.put("problem", problemVo);
			response.setElement(map);
		} else {
			response.setSuccess(false);
		}
		return response;
	}
}
