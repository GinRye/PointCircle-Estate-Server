package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_house")
public class House extends JpaBaseEntity<String> {

	@Id
	@Column(name = "house_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "house_name", nullable = false)
	private String houseName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "house_type_id", 
		foreignKey = @ForeignKey(name = "tb_house_house_type_id"))
	private HouseType houseType;
	
	@Column(name = "house_type_id", updatable = false, insertable = false)
	private String houseTypeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "floor_id", 
		foreignKey = @ForeignKey(name = "tb_house_floor_id"))
	private Floor floor;
	
	@Column(name = "floor_id", updatable = false, insertable = false)
	private String floorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit_id", 
		foreignKey = @ForeignKey(name = "tb_house_unit_id"))
	private Unit unit;
	
	@Column(name = "unit_id", updatable = false, insertable = false)
	private String unitId;
	
	@Column(name = "is_public_area")
	private boolean isPublicArea = false;
	
	@Column(name = "house_number")
	private String houseNumber;
	
	@Column(name = "house_complete_name")
	private String houseCompleteName;
}
