package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.entity.QualityMeasureCheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.QualityMeasureCheckQualityRepair;

public interface QualityMeasureCheckQualityRepairRepository extends IRepository<QualityMeasureCheckQualityRepair, String> {

	List<QualityMeasureCheckQualityRepair> findByQualityMeasureCheckQualityProblem(QualityMeasureCheckQualityProblem qualityMeasureCheckQualityProblem);
	
	List<QualityMeasureCheckQualityRepair> findByRepair(User repair);
	
	@Query("select o from QualityMeasureCheckQualityRepair o where o.qualityMeasureCheckQualityProblem in :qualityMeasureCheckQualityProblems")
	List<QualityMeasureCheckQualityRepair> findByQualityMeasureCheckQualityProblemIn(@Param("qualityMeasureCheckQualityProblems") List<QualityMeasureCheckQualityProblem> qualityMeasureCheckQualityProblems);

	@Query("select o from QualityMeasureCheckQualityRepair o where o.qualityMeasureCheckQualityProblem.building = :building")
	List<QualityMeasureCheckQualityRepair> findByBuilding(@Param("building") Building building);
}
