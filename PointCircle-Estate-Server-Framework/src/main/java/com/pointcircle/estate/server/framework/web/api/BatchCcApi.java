package com.pointcircle.estate.server.framework.web.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.core.web.IFindByIdRest;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.BatchCc;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;
import com.pointcircle.estate.server.framework.repository.BatchCcRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityBatchRepository;

@RestController
@RequestMapping("/api/batchCc")
public class BatchCcApi implements
	IFindAllRest<BatchCc, String, BatchCcApi.BatchCcEntityVo>,
	IFindByIdRest<BatchCc, String, BatchCcApi.BatchCcEntityVo> {

	@Autowired
	private BatchCcRepository repository;
	
	@Autowired
	private CheckQualityBatchRepository checkQualityBatchRepository;
	
	public class BatchCcEntityVo extends EntityVo<BatchCc, String> {

		public BatchCcEntityVo(BatchCc entity) {
			super(entity);
		}
		@JsonProperty("batchId")
		public String getBatchId() {
			return entity.getBatchId();
		}
		@JsonProperty("userId")
		public String getUserId() {
			return entity.getUserId();
		}
	}

	@Override
	public IRepository<BatchCc, String> getRepository() {
		return repository;
	}
	
	@GetMapping("findByCheckQualityBatch")
	public RestResponseList findByCheckQualityBatch(
		@RequestParam("checkQualityBatchId") String CheckQualityBatchId) {
		CheckQualityBatch checkQualityBatch = checkQualityBatchRepository.findOne(CheckQualityBatchId);
		List<BatchCc> BatchCcs = repository.findByCheckQualityBatch(checkQualityBatch);
		return this.createRestResponseList(BatchCcs);
	}
}
