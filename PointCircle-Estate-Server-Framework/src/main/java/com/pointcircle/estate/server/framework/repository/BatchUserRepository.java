package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BatchUser;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;

@Repository
public interface BatchUserRepository extends IRepository<BatchUser, String> {

	List<BatchUser> findByCheckQualityBatch(CheckQualityBatch batch);
	
	@Query("select o from BatchUser o")
	List<BatchUser> findAll();
	
	@Query("select o from BatchUser o where o.checkQualityBatch in :checkQualityBatchs")
	List<BatchUser> findBycheckQualityBatchIn(@Param("checkQualityBatchs") List<CheckQualityBatch> checkQualityBatchs);
}
