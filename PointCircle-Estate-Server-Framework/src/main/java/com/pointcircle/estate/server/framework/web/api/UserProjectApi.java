package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.UserProject;
import com.pointcircle.estate.server.framework.repository.UserProjectRepository;

@RestController
@RequestMapping("/api/userProject")
public class UserProjectApi implements 
	IFindAllRest<UserProject, String, UserProjectApi.UserProjectEntityVo> {
	
	@Autowired
	private UserProjectRepository userProjectRepository;
	
	public class UserProjectEntityVo extends EntityVo<UserProject, String> {
		public UserProjectEntityVo(UserProject entity) {
			super(entity);
		}
		@JsonProperty("userId")
		public String userId() {
			return entity.getUserId();
		}
		@JsonProperty("projectId")
		public String projectId() {
			return entity.getProjectId();
		}
	}

	@Override
	public IRepository<UserProject, String> getRepository() {
		return userProjectRepository;
	}
}
