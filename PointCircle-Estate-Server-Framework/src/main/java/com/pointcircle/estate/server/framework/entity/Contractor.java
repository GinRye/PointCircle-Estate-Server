package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_contractor")
public class Contractor extends JpaBaseEntity<String>{
	
	
	@Id 
	@Column(name = "contractor_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "contractor_name")
	private String contractorName;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "contractor_short_name")
	private String contractorShortName;

	@Column(name = "manager")
	private String manager;
	
	@Column(name = "contacter")
	private String contacter;
	
	@Column(name = "contact_phone")
	private String contactPhone;
	
	@Column(name = "user_amount")
	private Integer userAmount;
	
	@Column(name = "social_credit_code", columnDefinition="varchar(255) default ''")
	private String socialCreditCode;
	
	@Column(name = "registration_number", columnDefinition="varchar(255) default ''")
	private String registrationNumber;
	
	@ManyToOne
	@JoinColumn(name = "contractor_type_id",
		foreignKey = @ForeignKey(name = "fk_tb_contractor_contractor_type_id"))
	private ContractorType contractorType;
	
	@Column(name = "contractor_type_id", updatable = false, insertable = false)
	private String contractorTypeId;
}
