package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Unit;
import com.pointcircle.estate.server.framework.repository.UnitRepository;

@RestController
@RequestMapping("/api/unit")
public class UnitApi implements IFindAllRest<Unit, String, UnitApi.UnitEntityVo> {

	@Autowired
	private UnitRepository unitRepository;
	
	public class UnitEntityVo extends EntityVo<Unit, String> {

		public UnitEntityVo(Unit entity) {
			super(entity);
		}
		@JsonProperty("unitName")
		public String getUnitName() {
			return entity.getUnitName();
		}
		@JsonProperty("buildingId")
		public String getBuildingId() {
			return entity.getBuildingId();
		}
		@JsonProperty("houseNumberList")
		public String houseNumberList() {
			return entity.getHouseNumberList();
		}
		@JsonProperty("sortNumber")
		public int getSortNumber() {
			return entity.getSortNumber();
		}
		@JsonProperty("doorAmount")
		public int doorAmount() {
			return entity.getDoorAmount();
		}
	}
	
	@Override
	public IRepository<Unit, String> getRepository() {
		return unitRepository;
	}
}
