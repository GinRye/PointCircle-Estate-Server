package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.CheckItemGuideline;
import com.pointcircle.estate.server.framework.repository.CheckItemGuidelineRepository;

@RestController
@RequestMapping("/api/checkItemGuideline")
public class CheckItemGuidelineApi implements 
	IFindAllRest<CheckItemGuideline, String, CheckItemGuidelineApi.CheckItemGuidelineEntityVo> {
	
	@Autowired
	private CheckItemGuidelineRepository checkItemGuidelineRepository;
	
	@Override
	public IRepository<CheckItemGuideline, String> getRepository() {
		return checkItemGuidelineRepository;
	}

	public class CheckItemGuidelineEntityVo extends EntityVo<CheckItemGuideline, String> {

		public CheckItemGuidelineEntityVo(CheckItemGuideline entity) {
			super(entity);
		}
		@JsonProperty("problemClassId")
		public String getProblemClassId() {
			return entity.getItemId();
		}
		@JsonProperty("content")
		public String getContent() {
			return entity.getContent();
		}
		@JsonProperty("passrateMode")
		public String getPassrateMode() {
			if(entity.getPassrateMode() == null) {
				return "1";
			}else {
				return entity.getPassrateMode();
			}
		}
		@JsonProperty("standardMax")
		public String getStandardMax() {
			return String.valueOf(entity.getStandardMax());
		}
		@JsonProperty("standardMin")
		public String getStandardMin() {
			return String.valueOf(entity.getStandardMin());
		}
		@JsonProperty("measurementAreaNum")
		public Integer getMeasurementAreaNum() {
			if(entity.getMeasurementAreaNum() == null) {
				return 3;
			}
			return entity.getMeasurementAreaNum();
		}
		@JsonProperty("perAreaPoints")
		public Integer getPerAreaPoints() {
			if(entity.getPerAreaPoints() == null) {
				return 5;
			}
			return entity.getPerAreaPoints();
		}
		@JsonProperty("processMode")
		public String getProcessMode() {
			if(entity.getProcessMode() == null) {
				return "1";
			}else {
				return entity.getProcessMode();
			}
		}
	}
}
