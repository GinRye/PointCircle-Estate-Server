package com.pointcircle.estate.server.framework.repository;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.CheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.CheckQualityRepair;

@Repository
public interface CheckQualityRepairRepository extends IRepository<CheckQualityRepair, String> {

	List<CheckQualityRepair> findByCheckQualityProblem(CheckQualityProblem checkQualityProblem);
	/*
	@Query("select o from CheckQualityRepair o where o.repair = :repair and o.")
	List<CheckQualityRepair> findByRepair(@Param("repair") User repair);
	*/
	
	// @Query("select o from CheckQualityRepair o where o.checkQualityProblem in :checkQualityProblems")
	default List<CheckQualityRepair> findByCheckQualityProblemIn(List<CheckQualityProblem> checkQualityProblems){
		if(CollectionUtils.isEmpty(checkQualityProblems)) {
			return new LinkedList<CheckQualityRepair>();
		}
		return this.findAll(new Specification<CheckQualityRepair>() {
			private static final long serialVersionUID = 1422264764241191344L;

			@Override
			public Predicate toPredicate(Root<CheckQualityRepair> root, CriteriaQuery<?> query,
				CriteriaBuilder cb) {
				query.where(
					root.get("checkQualityProblem").in(checkQualityProblems)
				);
				return query.getRestriction();
			}
			
		});
	}
	
	
	List<CheckQualityRepair> findByRepair(User repair);
}
