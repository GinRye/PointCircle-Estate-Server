package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.repository.BidSectionRepository;

@RestController
@RequestMapping("/api/bidSection")
public class BidSectionApi implements 
	IFindAllRest<BidSection, String, BidSectionApi.BidSectionEntityVo> {
	
	@Autowired
	private BidSectionRepository bidSectionRepository;
	
	public class BidSectionEntityVo extends EntityVo<BidSection, String> {
		public BidSectionEntityVo(BidSection entity) {
			super(entity);
		}
		@JsonProperty("bidSectionName")
		public String getBidSectionName() {
			return entity.getBidSectionName();
		}
		@JsonProperty("projectId")
		public String getProjectId() {
			return entity.getProjectId();
		}
		@JsonProperty("contractorId")
		public String getContractorId() {
			return entity.getContractorId();
		}
		@JsonProperty("supervisorId")
		public String getSupervisorId() {
			return entity.getSupervisorId();
		}
	}

	@Override
	public IRepository<BidSection, String> getRepository() {
		return bidSectionRepository;
	}
}
