package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.repository.BuildingRepository;

@RestController
@RequestMapping("/api/building")
public class BuildingApi implements IFindAllRest<Building, String, BuildingApi.BuildingEntityVo> {

	@Autowired
	private BuildingRepository buildingRepository;
	
	public class BuildingEntityVo extends EntityVo<Building, String> {
		public BuildingEntityVo(Building entity) {
			super(entity);
		}
		@JsonProperty("buildingName")
		public String getBuildingName() {
			return entity.getBuildingName();
		}
		@JsonProperty("sortNumber")
		public int getSortNumber() {
			return entity.getSortNumber();
		}
		@JsonProperty("projectId")
		public String getProjectId() {
			return entity.getProjectId();
		}
		@JsonProperty("bidSectionId")
		public String getBidSectionId() {
			return entity.getBidSectionId();
		}
	}

	@Override
	public IRepository<Building, String> getRepository() {
		return buildingRepository;
	}
}
