package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Project;

@Repository
public interface ProjectRepository extends IRepository<Project, String> {
	
	Project findByProjectName(String projectName);
	
	List<Project> findByParentProject(Project parentProject);
	
	@Query(value = "select * from tb_project where parent_project_id is not null;", nativeQuery = true)
	List<Project> findByParentIdNotNull();
}
