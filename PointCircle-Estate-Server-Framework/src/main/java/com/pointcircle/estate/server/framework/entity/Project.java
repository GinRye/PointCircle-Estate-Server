package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_project")
public class Project extends JpaBaseEntity<String> {

	@Id
	@Column(name = "project_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "project_name", nullable = false)
	private String projectName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_project_id",
		foreignKey = @ForeignKey(name = "fk_tb_project_parent_project_id"))
	private Project parentProject;
	
	@Column(name = "parent_project_id", updatable = false, insertable = false)
	private String parentProjectId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id",
		foreignKey = @ForeignKey(name = "fk_tb_project_organization_id"))
	private Organization organization;
	
	@Column(name = "organization_id", updatable = false, insertable = false)
	private String organizationId;
}
