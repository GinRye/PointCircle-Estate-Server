package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.BidSectionCheckItem;
import com.pointcircle.estate.server.framework.repository.BidSectionCheckItemRepository;

@RestController
@RequestMapping("/api/bidSectionCheckItem")
public class BidSectionCheckItemApi implements
	IFindAllRest<BidSectionCheckItem, String, BidSectionCheckItemApi.BidSectionCheckItemEntityVo>{
	
	@Autowired
	private BidSectionCheckItemRepository bidSectionCheckItemRepository;
	
	@Override
	public IRepository<BidSectionCheckItem, String> getRepository() {
		return bidSectionCheckItemRepository;
	}
	
	public class BidSectionCheckItemEntityVo extends EntityVo<BidSectionCheckItem, String>{

		public BidSectionCheckItemEntityVo(BidSectionCheckItem entity) {
			super(entity);
		}
		@JsonProperty("bidSectionId")
		public String getBidSectionId() {
			return entity.getBidSectionId();
		}
		
		@JsonProperty("checkItemId")
		public String checkItemId() {
			return entity.getCheckItemId();
		}
		
		@JsonProperty("contractorId")
		public String getContractorId() {
			return entity.getContractorId();
		}
		
		@JsonProperty("repair")
		public String repair() {
			return entity.getRepairId();
		}
		@JsonProperty("reinspect")
		public String reinspect() {
			return entity.getReinspect();
		}
		@JsonProperty("cc")
		public String getCc() {
			return entity.getCc(); 
		}
		@JsonProperty("checkItemType")
		public String getCheckItemType() {
			return entity.getCheckItemType();
		}
	}
}
