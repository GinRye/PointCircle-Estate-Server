package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.entity.Floor;
import com.pointcircle.estate.server.framework.entity.House;
import com.pointcircle.estate.server.framework.entity.HouseType;
import com.pointcircle.estate.server.framework.entity.Unit;

@Repository
public interface HouseRepository extends IRepository<House, String> {

	List<House> findByHouseType(HouseType houseType);
	
	@Query("select o from House o where o.unit = :unit")
	List<House> findByUnit(@Param("unit") Unit unit);

	@Query("select o from House o where o.floor = :floor order by ABS(house_number)")
	List<House> findByFloor(@Param("floor") Floor floor);
	
	@Query("select o from House o where o.unit.building = :building")
	List<House> findByBuilding(@Param("building") Building building);
	
	List<House> findByUnitAndFloor(@Param("unit") Unit unit, @Param("floor") Floor floor);

	House findByUnitAndFloorAndHouseName(
		@Param("unit") Unit unit, 
		@Param("floor") Floor floor,
		@Param("houseName") String houseName);
}
