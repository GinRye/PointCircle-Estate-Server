package com.pointcircle.estate.server.framework.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ids {

	List<String> problemIds;
	
}
