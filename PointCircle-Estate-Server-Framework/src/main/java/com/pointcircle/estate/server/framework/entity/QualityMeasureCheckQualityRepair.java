package com.pointcircle.estate.server.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_quality_measure_checkquality_repair")
public class QualityMeasureCheckQualityRepair extends JpaBaseEntity<String> {

	@Id
	@Column(name = "quality_measure_checkquality_repair_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quality_measure_checkquality_problem_id", nullable = false)
	private QualityMeasureCheckQualityProblem qualityMeasureCheckQualityProblem;
	
	@Column(name = "quality_measure_checkquality_problem_id", updatable = false, insertable = false)
	private String qualityMeasureCheckqualityProblemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "repair", nullable = false)
	private User repair;

	@Column(name = "repair", updatable = false, insertable = false)
	private String repairId;
	
	@Column(name = "has_read")
	private Boolean hasRead = false;
	
	@Column(name = "reading_time")
	private Date readingTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_qualityMeasureCheckQualityRepair_id")
	private QualityMeasureCheckQualityRepair parentQualityMeasureCheckQualityRepair;

	@Column(name = "parent_qualityMeasureCheckQualityRepair_id", updatable = false, insertable = false)
	private String parentQualityMeasureCheckQualityRepairId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "child_qualityMeasureCheckQualityRepair_id")
	private QualityMeasureCheckQualityRepair childQualityMeasureCheckQualityRepair;

	@Column(name = "child_qualityMeasureCheckQualityRepair_id", updatable = false, insertable = false)
	private String childQualityMeasureCheckQualityRepairId;
}
