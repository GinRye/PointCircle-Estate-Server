package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.Contractor;

@Repository("contractorRepository")
public interface ContractorRepository extends IRepository<Contractor, String>{
	
	@Query("select o.user from ContractorUser o where o.contractor = :contractor")
	List<User> findUsersByContractor(@Param("contractor") Contractor contractor);
	
	List<Contractor> findAll();

}
