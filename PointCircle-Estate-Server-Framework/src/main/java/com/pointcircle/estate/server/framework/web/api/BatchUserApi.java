package com.pointcircle.estate.server.framework.web.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.core.web.IFindByIdRest;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.BatchUser;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;
import com.pointcircle.estate.server.framework.repository.BatchUserRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityBatchRepository;

@RestController
@RequestMapping("/api/batchUser")
public class BatchUserApi  implements
	IFindAllRest<BatchUser, String, BatchUserApi.BatchUserEntityVo>,
	IFindByIdRest<BatchUser, String, BatchUserApi.BatchUserEntityVo> {

	@Autowired
	private BatchUserRepository repository;
	
	@Autowired
	private CheckQualityBatchRepository checkQualityBatchRepository;
	
	public class BatchUserEntityVo extends EntityVo<BatchUser, String> {

		public BatchUserEntityVo(BatchUser entity) {
			super(entity);
		}
		@JsonProperty("batchId")
		public String getBatchId() {
			return entity.getBatchId();
		}
		@JsonProperty("userId")
		public String getUserId() {
			return entity.getUserId();
		}
	}

	@Override
	public IRepository<BatchUser, String> getRepository() {
		return repository;
	}
	
	@GetMapping("findByCheckQualityBatch")
	public RestResponseList findByCheckQualityBatch(
		@RequestParam("checkQualityBatchId") String CheckQualityBatchId) {
		CheckQualityBatch checkQualityBatch = checkQualityBatchRepository.findOne(CheckQualityBatchId);
		List<BatchUser> BatchUsers = repository.findByCheckQualityBatch(checkQualityBatch);
		return this.createRestResponseList(BatchUsers);
	}
}
