package com.pointcircle.estate.server.framework.web.api;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.core.repository.UserRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.core.web.RestException;
import com.pointcircle.core.web.RestResponse;
import com.pointcircle.core.web.annotation.OperationLogAnnotation;
import com.pointcircle.estate.server.framework.entity.UserInfo;
import com.pointcircle.estate.server.framework.repository.UserInfoRepository;
import com.pointcircle.estate.server.framework.service.common.IEncryptService;


@RestController
@RequestMapping("/api/user")
public class UserApi implements IFindAllRest<User, String, UserApi.UserEntityVo> {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private IEncryptService encryptService;
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	
	public class UserEntityVo extends EntityVo<User, String> {
		private UserInfo userInfo;
		public UserEntityVo(User entity) {
			super(entity);
			this.userInfo = userInfoRepository.findByUser(entity);
		}
		@JsonProperty("effectiveDate")
		public Long effectiveDate() {
			return userInfo != null && userInfo.getEffectiveDate() != null ? userInfo.getEffectiveDate().getTime() : null;
		}
		@JsonProperty("username")
		public String getUsername() {
			return entity.getUsername();
		}
		@JsonProperty("realname")
		public String getRealname() {
			return entity.getRealname();
		}
		@JsonProperty("email")
		public String getEmail() {
			return userInfo != null ? userInfo.getEmail() : null;
		}
		@JsonProperty("mobilephone")
		public String getMobilephone() {
			return userInfo != null ? userInfo.getMobilephone() : null;
		}
		@JsonProperty("organizationId")
		public String getOrganizationId() {
			return userInfo != null ? userInfo.getOrganizationId() : null;
		}
		@JsonProperty("appPermission")
		public String getAppPermission() {
			return userInfo != null ? userInfo.getAppPermission() : null;
		}
	}

	@Override
	public IRepository<User, String> getRepository() {
		return userRepository;
	}
	
	@OperationLogAnnotation
	@PostMapping("mobileChangePassword")
	public RestResponse mobileChangePassword(
		@RequestParam("userId") String userId,
		@RequestParam("oldPassword") String oldPassword,
		@RequestParam("password") String password) {
		
		String oldPasswordHex = encryptService.encrypt(oldPassword);
		String passwordHex = encryptService.encrypt(password);
		User user = userRepository.findOne(userId);
		if(!StringUtils.equals(oldPasswordHex, user.getPasswordHex())) {
			throw new RestException("原密码输入错误！");
		}else if(StringUtils.equals(passwordHex, user.getPasswordHex())) {
			throw new RestException("新密码与原密码一致！");
		}else{
			String hex = encryptService.encrypt(password);
			user.setPasswordHex(hex);
			userRepository.saveAndFlush(user);
			RestResponse response = new RestResponse();
			response.setSuccess(true);
			return response;
		}
	
	}
}
