package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Organization;

@Repository("organizationRepository")
public interface OrganizationRepository extends IRepository<Organization, String> {
	
	List<Organization> findByParentOrganization(Organization parentOrganization);
	
	Organization findByParentOrganizationAndOrganizationName(
		@Param("organization") Organization organization,
		@Param("organizationName") String organizationName
	);
}
