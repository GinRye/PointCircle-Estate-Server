package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "tb_contractor_user")
public class ContractorUser extends JpaBaseEntity<String>{
	
	@Id
	@Column(name = "contractor_user_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "contractor_id",
		foreignKey = @ForeignKey(name = "fk_tb_contractor_user_contractor_id"))
	private  Contractor contractor;
	
	@ManyToOne
	@JoinColumn(name = "user_id",
		foreignKey = @ForeignKey(name = "fk_tb_contractor_user_user_id"))
	private User user;
}
