package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.ProblemClass;

@Repository("problemClassRepository")
public interface ProblemClassRepository extends IRepository<ProblemClass, String> {
	
	@Query("select o from ProblemClass o")
	List<ProblemClass> findAll();
	
	@Query("select o from ProblemClass o where o.parentProblemClass is null order by o.problemClassName asc")
	List<ProblemClass> findRoot();
	
	@Query("select o from ProblemClass o where o.parentProblemClass = :parentProblemClass order by o.problemClassName asc")
	List<ProblemClass> findProblemClassByParentProblemClass(@Param("parentProblemClass") ProblemClass parentProblemClass);
	
	@Query("select o from ProblemClass o where o.id = :problemClassId")
	ProblemClass findProblemClassById(@Param("problemClassId") String problemClassId);
	
	@Query("select o from ProblemClass o where o.code = :code")
	ProblemClass findProblemClassByCode(@Param("code") String code);

}
