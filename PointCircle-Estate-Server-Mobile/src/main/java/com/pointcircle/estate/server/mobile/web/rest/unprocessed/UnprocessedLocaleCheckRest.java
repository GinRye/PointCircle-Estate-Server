package com.pointcircle.estate.server.mobile.web.rest.unprocessed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pointcircle.core.entity.User;
import com.pointcircle.core.repository.UserRepository;
import com.pointcircle.core.web.RestResponseElement;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.BatchType;
import com.pointcircle.estate.server.framework.entity.CheckQualityProblem;
import com.pointcircle.estate.server.framework.repository.CheckQualityCcRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityProblemRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityReinspectRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityRepairRepository;
import com.pointcircle.estate.server.framework.web.api.CheckQualityProblemApi;
import com.pointcircle.estate.server.framework.web.api.CheckQualityProblemApi.CheckQualityProblemEntityVo;

@RestController
@RequestMapping("/rest/unprocessed/localecheck/v3")
@CrossOrigin("*")
public class UnprocessedLocaleCheckRest {
	
	@Autowired
	CheckQualityProblemRepository checkQualityProblemRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	CheckQualityProblemApi checkQualityProblemApi;
	
	@Autowired
	CheckQualityRepairRepository checkQualityRepairRepository;
	
	@Autowired
	CheckQualityReinspectRepository checkQualityReinspectRepository;
	
	@Autowired
	CheckQualityCcRepository checkQualityCcRepository;
	
	
	@GetMapping("findLocalCheckProblemByUser")
	@Transactional
	public RestResponseList findLocalCheckProblemOfUnprocessedByUser(
			String userId) {
		List<CheckQualityProblem> problems = checkQualityProblemRepository.findProblemByUser(userId);
		
		List<CheckQualityProblem> tempProblems = new ArrayList<>();;	
		List<CheckQualityProblemEntityVo> problemVos = new ArrayList<>();
		List<String> problemIds = new ArrayList<String>();
		
		for(CheckQualityProblem problem : problems) {
			if(problem.getBatchType() == BatchType.安全文明 && 
					!problem.getProblemClass().getParentProblemClass().getProblemClassName().equals("工程亮点")) {
				if(!problemIds.contains(problem.getId())) {
					problemIds.add(problem.getId());
					tempProblems.add(problem);
				}
			}
		}
		
		if(tempProblems.size() > 0) {
			Map<String, List<String>> processedCcMap = new HashMap<String, List<String>>();
			checkQualityCcRepository.findByCheckQualityProblemIn(tempProblems)
				.forEach((cc) -> {
					List<String> ccIds = processedCcMap.get(cc.getCheckqualityProblemId());
					if(ccIds == null) {
						ccIds = new LinkedList<String>();
						processedCcMap.put(cc.getCheckqualityProblemId(), ccIds);
					}
					ccIds.add(cc.getCcId());
				});
			problemVos =
					tempProblems.stream().map((problem) -> {
						CheckQualityProblemEntityVo vo = checkQualityProblemApi.new CheckQualityProblemEntityVo(problem);
						vo.setCcIds(processedCcMap.get(problem.getId()));
						return vo;
					}).collect(Collectors.toList());
		}
		RestResponseList response = new RestResponseList();
		response.setList(problemVos);
		return response;
	}
	
	@GetMapping("findLocalCheckCcProblemsByUser")
	public RestResponseElement findLocalCheckCcProblemsByUser(
			@RequestParam (value = "userId") String userId,
			@RequestParam(value = "startDate", required = false) Date startDate,
			@RequestParam(value = "endDate", required = false) Date endDate) {
		User user = userRepository.findOne(userId);
		GregorianCalendar calendar = new GregorianCalendar();
		
		// 开始时间  默认7天
		if(startDate == null) {
			Date now = new Date();
			calendar.setTime(now);
			calendar.add(Calendar.DAY_OF_MONTH, -7); 
			startDate = calendar.getTime();
		}else{
			calendar.setTime(startDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			startDate = calendar.getTime();
		}
		// 结束时间   默认为当前时间
		if(endDate == null) {
			Date now = new Date();
			calendar.setTime(now);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			endDate = calendar.getTime();
		}else {
			calendar = new GregorianCalendar();
			calendar.setTime(endDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			endDate = calendar.getTime();
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		List<CheckQualityProblem> ccs = checkQualityProblemRepository.findByCcAndTwoDate(
				user.getId(), dateFormat.format(startDate), dateFormat.format(endDate));
		
		List<CheckQualityProblemEntityVo> problemEntityVoCcList = new ArrayList<>();
		Map<String, String> unprocessedRepairMap = new HashMap<String, String>();
		Map<String, List<String>> unprocessedReinspectMap = new HashMap<String, List<String>>();
		Map<String, List<String>> unprocessedCcMap = new HashMap<String, List<String>>();
		checkQualityRepairRepository.findByCheckQualityProblemIn(ccs)
		.forEach((repair) -> {
			unprocessedRepairMap.put(repair.getCheckqualityProblemId(), repair.getRepairId());
		});
		checkQualityReinspectRepository.findByCheckQualityProblemIn(ccs)
		.forEach((respinect) -> {
			List<String> respinectIds = unprocessedReinspectMap.get(respinect.getCheckqualityProblemId());
			if(respinectIds == null) {
				respinectIds = new LinkedList<String>();
				unprocessedReinspectMap.put(respinect.getCheckqualityProblemId(), respinectIds);
			}
			respinectIds.add(respinect.getReinspectId());
		});
		checkQualityCcRepository.findByCheckQualityProblemIn(ccs)
			.forEach((cc) -> {
				List<String> ccIds = unprocessedCcMap.get(cc.getCheckqualityProblemId());
				if(ccIds == null) {
					ccIds = new LinkedList<String>();
					unprocessedCcMap.put(cc.getCheckqualityProblemId(), ccIds);
				}
				ccIds.add(cc.getCcId());
			});
		problemEntityVoCcList = 
				ccs.stream().map((problem) -> {
					CheckQualityProblemEntityVo vo = checkQualityProblemApi.new CheckQualityProblemEntityVo(problem);
					vo.setRepairId(unprocessedRepairMap.get(problem.getId()));
					vo.setReinspectIds(unprocessedReinspectMap.get(problem.getId()));
					vo.setCcIds(unprocessedCcMap.get(problem.getId()));
					return vo;
				}).collect(Collectors.toList());
		RestResponseElement response = new RestResponseElement();
		Map<String, Object> map = new HashMap<>();
		map.put("ccProblem", problemEntityVoCcList);
		map.put("startTime", startDate);
		map.put("endTime", endDate);
		response.setElement(map);
		return response;
		
	}
	
	/*
	@GetMapping("findLocalCheckCcProblemsByUser")
	public RestResponseList findLocalCheckCcProblemsByUser(@RequestParam (value = "userId") String userId){
		
		List<CheckQualityProblem> ccs = checkQualityProblemRepository.findByCc(userId);
		List<CheckQualityProblemEntityVo> problemEntityVoCcList = new ArrayList<>();
		Map<String, String> unprocessedRepairMap = new HashMap<String, String>();
		Map<String, List<String>> unprocessedReinspectMap = new HashMap<String, List<String>>();
		Map<String, List<String>> unprocessedCcMap = new HashMap<String, List<String>>();
		checkQualityRepairRepository.findByCheckQualityProblemIn(ccs)
		.forEach((repair) -> {
			unprocessedRepairMap.put(repair.getCheckqualityProblemId(), repair.getRepairId());
		});
		checkQualityReinspectRepository.findByCheckQualityProblemIn(ccs)
		.forEach((respinect) -> {
			List<String> respinectIds = unprocessedReinspectMap.get(respinect.getCheckqualityProblemId());
			if(respinectIds == null) {
				respinectIds = new LinkedList<String>();
				unprocessedReinspectMap.put(respinect.getCheckqualityProblemId(), respinectIds);
			}
			respinectIds.add(respinect.getReinspectId());
		});
		checkQualityCcRepository.findByCheckQualityProblemIn(ccs)
			.forEach((cc) -> {
				List<String> ccIds = unprocessedCcMap.get(cc.getCheckqualityProblemId());
				if(ccIds == null) {
					ccIds = new LinkedList<String>();
					unprocessedCcMap.put(cc.getCheckqualityProblemId(), ccIds);
				}
				ccIds.add(cc.getCcId());
			});
		problemEntityVoCcList = 
				ccs.stream().map((problem) -> {
					CheckQualityProblemEntityVo vo = checkQualityProblemApi.new CheckQualityProblemEntityVo(problem);
					vo.setRepairId(unprocessedRepairMap.get(problem.getId()));
					vo.setReinspectIds(unprocessedReinspectMap.get(problem.getId()));
					vo.setCcIds(unprocessedCcMap.get(problem.getId()));
					return vo;
				}).collect(Collectors.toList());
		
		RestResponseList response = new RestResponseList();
		response.setList(problemEntityVoCcList);
		return response;
	}*/
		
}
