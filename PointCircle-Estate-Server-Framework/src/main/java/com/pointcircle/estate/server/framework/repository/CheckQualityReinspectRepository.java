package com.pointcircle.estate.server.framework.repository;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.CheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.CheckQualityReinspect;

@Repository
public interface CheckQualityReinspectRepository extends IRepository<CheckQualityReinspect, String> {

	List<CheckQualityReinspect> findByCheckQualityProblem(CheckQualityProblem checkQualityProblem);
	
	List<CheckQualityReinspect> findByReinspect(User reinspect);
	
	// @Query("select o from CheckQualityReinspect o where o.checkQualityProblem in :checkQualityProblems")
	default List<CheckQualityReinspect> findByCheckQualityProblemIn(List<CheckQualityProblem> checkQualityProblems){
		if(CollectionUtils.isEmpty(checkQualityProblems)) {
			return new LinkedList<CheckQualityReinspect>();
		}
		return this.findAll(new Specification<CheckQualityReinspect>() {
			private static final long serialVersionUID = 3068349777501665513L;

			@Override
			public Predicate toPredicate(Root<CheckQualityReinspect> root, CriteriaQuery<?> query,
				CriteriaBuilder cb) {
				query.where(
					root.get("checkQualityProblem").in(checkQualityProblems)
				);
				return query.getRestriction();
			}
			
		});
	}
}
