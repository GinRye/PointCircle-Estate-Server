package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Problem;
import com.pointcircle.estate.server.framework.entity.ProblemClass;

@Repository("problemRepository")
public interface ProblemRepository extends IRepository<Problem, String> {
	
	@Query("select o from Problem o where o.problemClass = :problemClass")
	List<Problem> findByClass(@Param("problemClass") ProblemClass problemClass);
	
	@Query("select o from Problem o")
	List<Problem> findAll();
	
	List<Problem> findByProblemClass(ProblemClass problemClass);

}
