package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_house_type")
public class HouseType extends JpaBaseEntity<String> {

	@Id
	@Column(name = "house_type_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "house_type_name", nullable = false)
	private String houseTypeName;
	
	@Column(name = "house_type_structure")
	private String houseTypeStructure;
	
	@Column(name = "img_path")
	private String imgPath;
	
	@Column(name = "url")
	private String url;
}
