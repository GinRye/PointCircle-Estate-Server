package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.Role;
import com.pointcircle.estate.server.framework.entity.RoleAssociationRole;

@Repository("roleAssociationRoleRepository")
public interface RoleAssociationRoleRepository extends IRepository<RoleAssociationRole, String> {
	
	@Query("select o.externalRole from RoleAssociationRole o where o.role = :role")
	List<Role> findExternalRoleByRole(@Param("role") Role role);
	
	@Query("select o from RoleAssociationRole o where o.role = :role and o.externalRole = :externalRole")
	RoleAssociationRole findRoleAssociationRoleByRoleAndRole(@Param("role") Role role, @Param("externalRole") Role externalRole);
}
