package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_problem_class")
public class ProblemClass extends JpaBaseEntity<String> {
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "level")
	private int level;
	
	@Column(name = "problem_class_name")
	private String problemClassName;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "parent_problem_class_id",
		foreignKey = @ForeignKey(name = "fk_tb_problem_class_parent_problem_class_id"))
	private ProblemClass parentProblemClass;
	
	@Column(name = "parent_problem_class_id", updatable = false, insertable = false)
	private String parentProblemClassId;
}
