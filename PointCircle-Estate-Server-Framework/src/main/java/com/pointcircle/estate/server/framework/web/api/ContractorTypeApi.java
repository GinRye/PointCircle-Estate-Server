package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.ContractorType;
import com.pointcircle.estate.server.framework.repository.ContractorTypeRepository;

@RestController
@RequestMapping("/api/contractorType")
public class ContractorTypeApi implements
       IFindAllRest<ContractorType, String, ContractorTypeApi.ContractorTypeEntityVo> {

	@Autowired
	private ContractorTypeRepository contractorTypeRepository;
	
	public class ContractorTypeEntityVo extends EntityVo<ContractorType, String> {
		public ContractorTypeEntityVo(ContractorType entity) {
			super(entity);
		}
		@JsonProperty("contractorTypeName")
		public String getContractorTypeName() {
			return entity.getContractorTypeName();
		}
	
	}

	@Override
	public IRepository<ContractorType, String> getRepository() {
		return contractorTypeRepository;
	}
}