package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.HouseType;
import com.pointcircle.estate.server.framework.repository.HouseTypeRepository;

@RestController
@RequestMapping("/api/houseType")
public class HouseTypeApi implements IFindAllRest<HouseType, String, HouseTypeApi.HouseTypeEntityVo> {

	@Autowired
	private HouseTypeRepository houseTypeRepository;
	
	public class HouseTypeEntityVo extends EntityVo<HouseType, String> {
		public HouseTypeEntityVo(HouseType entity) {
			super(entity);
		}
		@JsonProperty("houseTypeName")
		public String getHouseTypeName() {
			return entity.getHouseTypeName();
		}
		@JsonProperty("houseTypeStructure")
		public String getHouseTypeStructure() {
			return entity.getHouseTypeStructure();
		}
		@JsonProperty("url")
		public String getUrl() {
			return entity.getUrl();
		}
	}

	@Override
	public IRepository<HouseType, String> getRepository() {
		return houseTypeRepository;
	}
}
