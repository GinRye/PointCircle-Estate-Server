package com.pointcircle.estate.server.framework.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.CheckItemGuideline;
import com.pointcircle.estate.server.framework.entity.ProblemClass;


@Repository("checkItemGuidelineRepository")
public interface CheckItemGuidelineRepository extends IRepository<CheckItemGuideline, String> {
	
	@Query("select o from CheckItemGuideline o where o.problemClass = :problemClass")
	CheckItemGuideline findByItem(@Param("problemClass") ProblemClass problemClass);
}
