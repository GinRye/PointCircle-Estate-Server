package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.Contractor;
import com.pointcircle.estate.server.framework.entity.ContractorUser;

@Repository("contractorUserRepository")
public interface ContractorUserRepository extends IRepository<ContractorUser, String>{
	
	List<User> findUsersByContractor(Contractor contractor);
	
	@Query("select o from ContractorUser o where o.contractor = :contractor")
	List<ContractorUser> findByContractor(@Param("contractor") Contractor contractor);
}
