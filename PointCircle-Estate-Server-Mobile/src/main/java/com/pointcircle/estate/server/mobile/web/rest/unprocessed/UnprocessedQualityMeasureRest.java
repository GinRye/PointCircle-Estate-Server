package com.pointcircle.estate.server.mobile.web.rest.unprocessed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pointcircle.core.entity.User;
import com.pointcircle.core.repository.UserRepository;
import com.pointcircle.core.web.RestResponse;
import com.pointcircle.core.web.RestResponseElement;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.BatchType;
import com.pointcircle.estate.server.framework.entity.QualityMeasureCheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.QualityMeasureCheckQualityRepair;
import com.pointcircle.estate.server.framework.repository.QualityMeasureCheckQualityCcRepository;
import com.pointcircle.estate.server.framework.repository.QualityMeasureCheckQualityProblemRepository;
import com.pointcircle.estate.server.framework.repository.QualityMeasureCheckQualityRepairRepository;
import com.pointcircle.estate.server.framework.web.api.QualityMeasureCheckQualityProblemApi;
import com.pointcircle.estate.server.framework.web.api.QualityMeasureCheckQualityProblemApi.QualityMeasureCheckQualityProblemEntityVo;

@RestController
@RequestMapping("/rest/unprocessed/qualitymeasure/v3")
@CrossOrigin("*")
public class UnprocessedQualityMeasureRest {
	
	@Autowired
	QualityMeasureCheckQualityProblemRepository qualityMeasureCheckQualityProblemRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	QualityMeasureCheckQualityRepairRepository qualityMeasureCheckQualityRepairRepository;
	
	@Autowired
	QualityMeasureCheckQualityCcRepository qualityMeasureCheckQualityCcRepository;
	
	@Autowired
	QualityMeasureCheckQualityProblemApi qualityMeasureCheckQualityProblemApi;
	
	@GetMapping("findQualityMeasureProblemsByUser")
	@Transactional
	public RestResponseList findQualityMeasureOfUnprocessedProblemsByUser(
			String userId) {
		User user = userRepository.findOne(userId);
		List<QualityMeasureCheckQualityProblem> registIsUser = qualityMeasureCheckQualityProblemRepository.findByRegistBy(user);
				
		List<String> ids = new ArrayList<String>();
		List<QualityMeasureCheckQualityProblem> list = new ArrayList<>();
		
		for(QualityMeasureCheckQualityProblem regist : registIsUser) {
			if(regist.getBatchType() == BatchType.实测实量 && !ids.contains(regist.getId())) {
				ids.add(regist.getId());
				list.add(regist);
			}
		}
		List<QualityMeasureCheckQualityProblemEntityVo> problemEntitytVoList = new ArrayList<>();
		if(list.size() > 0) {
			Map<String, List<String>> ccMap = new HashMap<String, List<String>>();
			qualityMeasureCheckQualityCcRepository.findByQualityMeasureCheckQualityProblemIn(list)
				.forEach((cc) -> {
					List<String> ccIds = ccMap.get(cc.getQualityMeasureCheckqualityProblemId());
					if(ccIds == null) {
						ccIds = new LinkedList<String>();
						ccMap.put(cc.getQualityMeasureCheckqualityProblemId(), ccIds);
					}
					ccIds.add(cc.getCcId());
				});
			problemEntitytVoList = 
					list.stream().map((problem) -> {
					QualityMeasureCheckQualityProblemEntityVo vo = 
							qualityMeasureCheckQualityProblemApi.new QualityMeasureCheckQualityProblemEntityVo(problem);
					vo.setCcIds(ccMap.get(problem.getId()));
					return vo;
				}).collect(Collectors.toList());
		}
		RestResponseList response = new RestResponseList();
		response.setList(problemEntitytVoList);
		return response;
	}
	
	
	@GetMapping("findQualityMeasureCcProblemsByUser")
	public RestResponseElement findProblemsByUser(
			@RequestParam (value = "userId") String userId,
			@RequestParam(value = "durationDay", required = false) Integer durationDay,
			@RequestParam(value = "startDate", required = false) Date startDate,
			@RequestParam(value = "endDate", required = false) Date endDate) {
		User user = userRepository.findOne(userId);
		GregorianCalendar calendar = new GregorianCalendar();
		Date now = new Date();
		// 开始时间  默认7天
		if(startDate == null) {
			calendar.setTime(now);
			if(durationDay == null) {
				calendar.add(Calendar.DAY_OF_MONTH, -7); 
				startDate = calendar.getTime();
			}else {
				calendar.add(Calendar.DAY_OF_MONTH, -durationDay);
				startDate = calendar.getTime();
			}
		}else{
			calendar.setTime(startDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			startDate = calendar.getTime();
		}
		// 结束时间   默认为当前时间
		if(endDate == null) {
			calendar.setTime(now);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			endDate = calendar.getTime();
		}else {
			calendar.setTime(endDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			endDate = calendar.getTime();
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		List<QualityMeasureCheckQualityProblem> ccs = qualityMeasureCheckQualityProblemRepository.findByCcAndTwoDate(user.getId(), 
				dateFormat.format(startDate), dateFormat.format(endDate));
		
		RestResponseElement response = new RestResponseElement();
		Map<String, Object> map = new HashMap<>();
		List<QualityMeasureCheckQualityProblemEntityVo> elements = 
			(List<QualityMeasureCheckQualityProblemEntityVo>) qualityMeasureCheckQualityProblemApi.createRestResponseList(ccs).getList();
		map.put("ccProblem", elements);
		map.put("startTime", startDate);
		map.put("endTime", endDate);
		response.setElement(map);
		return response;
		
	}
	
	@GetMapping("changeRepair")
	public RestResponse changeRepair(
			@RequestParam(value = "QualityMeasureProblemId") String QualityMeasureProblemId,
			@RequestParam(value = "newRepairId") String newRepairId) {
		QualityMeasureCheckQualityProblem qualityMeasureCheckQualityProblem = qualityMeasureCheckQualityProblemRepository.findOne(QualityMeasureProblemId);
		User newRepair = userRepository.findOne(newRepairId);
		List<QualityMeasureCheckQualityRepair> repairList = qualityMeasureCheckQualityRepairRepository.findByQualityMeasureCheckQualityProblem(qualityMeasureCheckQualityProblem);
		boolean isHaveRepair = false;
		QualityMeasureCheckQualityRepair tailRepair = null;
		for(QualityMeasureCheckQualityRepair repair : repairList) {
			if(repair.getRepair() == newRepair) {
				isHaveRepair = true;
			}if(repair.getChildQualityMeasureCheckQualityRepair() == null){
				tailRepair = repair;
			}
		}
		RestResponse response = new RestResponse();
		if(!isHaveRepair) {
			
			QualityMeasureCheckQualityRepair newQualityMeasureCheckQualityRepair = new QualityMeasureCheckQualityRepair();
			newQualityMeasureCheckQualityRepair.setQualityMeasureCheckQualityProblem(qualityMeasureCheckQualityProblem);
			newQualityMeasureCheckQualityRepair.setParentQualityMeasureCheckQualityRepair(tailRepair);
			newQualityMeasureCheckQualityRepair.setRepair(newRepair);
			qualityMeasureCheckQualityRepairRepository.saveAndFlush(newQualityMeasureCheckQualityRepair);
			tailRepair.setChildQualityMeasureCheckQualityRepair(newQualityMeasureCheckQualityRepair);
			qualityMeasureCheckQualityRepairRepository.saveAndFlush(tailRepair);
			qualityMeasureCheckQualityProblem.setUpdateTime(new Date());
			response.setSuccess(true);
		}else {
			response.setSuccess(false);
		}
		return response;
	}
	
	
}
