package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_building_house_type")
public class BuildingHouseType extends JpaBaseEntity<String> {

	@Id
	@Column(name = "building_house_type_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "building_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_building_house_type_building_id"))
	private Building building;
	
	@ManyToOne
	@JoinColumn(name = "house_type_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_building_house_type_house_type_id"))
	private HouseType houseType;
}
