package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.Project;
import com.pointcircle.estate.server.framework.entity.UserProject;

@Repository
public interface UserProjectRepository extends IRepository<UserProject, String> {
	
	List<UserProject> findByUser(User user);
	
	List<UserProject> findByProject(Project project);
	
	UserProject findByUserAndProject(User user, Project project);
	
	@Query("select o.user from UserProject o where o.project = :project")
	List<User> findUserByProject(@Param("project") Project project);
	
	@Query("select o.project from UserProject o where o.user = :user")
	List<Project> findProjectByUser(@Param("user") User user);
	
	@Query("select o from UserProject o where o.project = :project and o.user != :user")
	List<UserProject> findUserProjectByProjectExceptUser(@Param("project") Project project, @Param("user") User user);
	
}
