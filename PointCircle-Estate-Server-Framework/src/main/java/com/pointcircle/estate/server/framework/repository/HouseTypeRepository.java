package com.pointcircle.estate.server.framework.repository;

import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.HouseType;

@Repository
public interface HouseTypeRepository extends IRepository<HouseType, String> {

}
