package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BatchCc;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;


@Repository
public interface BatchCcRepository extends IRepository<BatchCc, String> {

	List<BatchCc> findByCheckQualityBatch(CheckQualityBatch batch);
	
	@Query("select o from BatchCc o")
	List<BatchCc> findAll();
	
	@Query("select o from BatchCc o where o.checkQualityBatch in :checkQualityBatchs")
	List<BatchCc> findBycheckQualityBatchIn(@Param("checkQualityBatchs") List<CheckQualityBatch> checkQualityBatchs);
}
