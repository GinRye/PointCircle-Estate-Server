package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_checkquality_batch")
public class CheckQualityBatch extends JpaBaseEntity<String> {

	@Id
	@Column(name = "checkquality_batch_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "batch_type")
	@Enumerated(EnumType.STRING)
	private BatchType batchType = BatchType.安全文明;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bid_section_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_batch_bid_section_id"))
	private BidSection bidSection;
	
	@Column(name = "bid_section_id", updatable = false, insertable = false)
	private String bidSectionId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "repair_deadline")
	private int repairDeadline;
	
	@Column(name = "is_close", nullable = false, columnDefinition = "tinyint(4) DEFAULT 0")
	private Byte isClose;
	
	@Column(name = "batch_purpose")
	@Enumerated(EnumType.STRING)
	private BatchPurpose batchPurpose;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "in_charge_by",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_batch_in_charge_by"))
	private User inChargeBy;
	
	@Column(name = "in_charge_by", updatable = false, insertable = false)
	private String inChargeById;
}
