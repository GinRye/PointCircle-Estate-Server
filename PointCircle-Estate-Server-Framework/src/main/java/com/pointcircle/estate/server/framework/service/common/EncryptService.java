package com.pointcircle.estate.server.framework.service.common;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Service;

@Service
public class EncryptService implements IEncryptService {

	private HashedCredentialsMatcher hashedCredentialsMatcher;
	
	private DefaultHashService hashService;

	@PostConstruct
	public void postConstruct() {
		hashedCredentialsMatcher = new HashedCredentialsMatcher();
		hashedCredentialsMatcher.setHashAlgorithmName("sha-1");
		hashedCredentialsMatcher.setHashIterations(1);
		hashService = new DefaultHashService();
		hashService.setHashAlgorithmName(hashedCredentialsMatcher.getHashAlgorithmName());
		hashService.setHashIterations(hashedCredentialsMatcher.getHashIterations());
	}
	
	@Override
	public String encrypt(String password) {
		HashRequest request = new HashRequest.Builder()
				.setAlgorithmName(hashedCredentialsMatcher.getHashAlgorithmName())
				.setSource(ByteSource.Util.bytes(password))
				.setIterations(hashedCredentialsMatcher.getHashIterations()).build();
		String hex = hashService.computeHash(request).toHex();
		return hex;
	}

}
