package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Problem;
import com.pointcircle.estate.server.framework.repository.ProblemRepository;

@RestController
@RequestMapping("/api/problem")
public class ProblemApi implements IFindAllRest<Problem, String, ProblemApi.ProblemEntityVo> {
	
	@Autowired
	private ProblemRepository problemRepository;
	
	@Override
	public IRepository<Problem, String> getRepository() {
		return problemRepository;
	}
	
	public class ProblemEntityVo extends EntityVo<Problem, String> {

		public ProblemEntityVo(Problem entity) {
			super(entity);
		}
		@JsonProperty("problemName")
		public String getProblemName() {
			return entity.getProblemName();
		}
		@JsonProperty("sort")
		public Integer getSort() {
			return entity.getSort();
		}
		@JsonProperty("classId")
		public String classId() {
			return entity.getClassId();
		}
	}
}
