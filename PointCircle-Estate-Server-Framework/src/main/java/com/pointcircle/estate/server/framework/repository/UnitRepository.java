package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.entity.Unit;

@Repository
public interface UnitRepository extends IRepository<Unit, String> {
	
	@Query("select o from Unit o where o.building = :building order by ABS(unitName)")
	List<Unit> findByBuilding(@Param("building") Building building);
	
	Unit findByBuildingAndUnitName(
		@Param("building") Building building, @Param("unitName") String unitName);
}
