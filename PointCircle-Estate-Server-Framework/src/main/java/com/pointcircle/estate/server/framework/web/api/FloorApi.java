package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Floor;
import com.pointcircle.estate.server.framework.repository.FloorRepository;

@RestController
@RequestMapping("/api/floor")
public class FloorApi implements IFindAllRest<Floor, String, FloorApi.FloorEntityVo> {
	
	@Autowired
	private FloorRepository floorRepository;
	
	public class FloorEntityVo extends EntityVo<Floor, String> {

		public FloorEntityVo(Floor entity) {
			super(entity);
		}
		@JsonProperty("floorName")
		public String getFloorName() {
			return entity.getFloorName();
		}
		@JsonProperty("buildingId")
		public String getBuildingId() {
			return entity.getBuildingId();
		}
		@JsonProperty("isPublicArea")
		public boolean getIsPublicArea() {
			return entity.isPublicArea();
		}
		@JsonProperty("isTop")
		public boolean getIsTop() {
			return entity.isTop();
		}
		@JsonProperty("sortNumber")
		public int getSortNumber() {
			return entity.getSortNumber();
		}
	}
	
	@Override
	public IRepository<Floor, String> getRepository() {
		return floorRepository;
	}
}
