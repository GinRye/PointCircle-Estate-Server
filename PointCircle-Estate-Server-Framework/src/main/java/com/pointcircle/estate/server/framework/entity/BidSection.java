package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_bid_section")
public class BidSection extends JpaBaseEntity<String>{
	
	@Id
	@Column(name = "bid_section_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "bid_section_name", nullable = false)
	private String bidSectionName;
	
	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false, 
	foreignKey = @ForeignKey(name = "fk_tb_bid_section_project_id"))
	private Project project;
	
	@Column(name = "project_id", updatable = false, insertable = false)
	private String projectId;
	
	@ManyToOne
	@JoinColumn(name = "supervisor_id",
	foreignKey = @ForeignKey(name = "fk_tb_bid_section_supervisor_id"))
	private Contractor supervisor;
	
	@Column(name = "supervisor_id", updatable = false, insertable = false)
	private String supervisorId;
	
	@ManyToOne
	@JoinColumn(name = "contractor_id",
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_contractor_id"))
	private Contractor contractor;
	
	@Column(name = "contractor_id", updatable = false, insertable = false)
	private String contractorId;
}
