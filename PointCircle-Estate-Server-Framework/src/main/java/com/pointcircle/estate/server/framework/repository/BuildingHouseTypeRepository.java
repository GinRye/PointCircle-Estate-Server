package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.entity.BuildingHouseType;
import com.pointcircle.estate.server.framework.entity.HouseType;
import com.pointcircle.estate.server.framework.entity.Project;

@Repository
public interface BuildingHouseTypeRepository extends IRepository<BuildingHouseType, String> {
	List<BuildingHouseType> findByHouseType(HouseType houseType);
	
	@Query("select o.houseType from BuildingHouseType o where UPPER(o.houseType.houseTypeName) like %:houseTypeName% "
			+ " and o.building.project in :projects")
	List<HouseType> findHouseTypeByHouseTypeNameLikeAndProjectIn(
		@Param("houseTypeName") String houseTypeName,
		@Param("projects") List<Project> projects
	);

	List<BuildingHouseType> findByBuilding(Building building);
}
