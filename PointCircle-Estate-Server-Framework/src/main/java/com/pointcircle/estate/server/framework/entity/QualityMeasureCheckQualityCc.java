package com.pointcircle.estate.server.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_quality_measure_checkquality_cc")
public class QualityMeasureCheckQualityCc extends JpaBaseEntity<String> {

	@Id
	@Column(name = "quality_measure_checkquality_cc_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quality_measure_checkquality_problem_id", nullable = false)
	private QualityMeasureCheckQualityProblem qualityMeasureCheckQualityProblem;

	@Column(name = "quality_measure_checkquality_problem_id", updatable = false, insertable = false)
	private String qualityMeasureCheckqualityProblemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cc", nullable = false)
	private User cc;
	
	@Column(name = "cc", updatable = false, insertable = false)
	private String ccId;

	@Column(name = "reading_status")
	private String readingStatus;
	
	@Column(name = "reading_time")
	private Date readingTime;
	
	@Column(name = "type")
	private String type;
}
