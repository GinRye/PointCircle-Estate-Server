package com.pointcircle.estate.server.framework.entity;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public enum BatchType {
	安全文明("安全文明"),
	实测实量("实测实量"),
	工序移交("工序移交"),
	工序验收("工序验收");
	
	@Getter
	private String label;
	
	private BatchType(String label) {
		this.label = label;
	}
	
	private static Map<String, BatchType> map = new HashMap<String, BatchType>();
	static {
		map.put("安全文明", 安全文明);
		map.put("实测实量", 实测实量);
		map.put("工序移交", 工序移交);
		map.put("工序验收", 工序验收);
	}
	
	public static BatchType convert(String value) {
		return map.get(value);
	}
}
