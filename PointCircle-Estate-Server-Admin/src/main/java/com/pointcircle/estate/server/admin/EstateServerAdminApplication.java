package com.pointcircle.estate.server.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.pointcircle.core", "com.pointcircle.estate.server.framework", "com.pointcircle.estate.server.admin"})
public class EstateServerAdminApplication {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(EstateServerAdminApplication.class, args);
	}
}
