package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_problem")
public class Problem extends JpaBaseEntity<String> {
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "sort")
	private int sort;
	
	@Column(name = "problem_name")
	private String problemName;

	@ManyToOne
	@JoinColumn(name = "class_id",
		foreignKey = @ForeignKey(name = "fk_tb_problem_problem_class_id"))
	private ProblemClass problemClass;
	
	@Column(name = "class_id", updatable = false, insertable = false)
	private String classId;
}
