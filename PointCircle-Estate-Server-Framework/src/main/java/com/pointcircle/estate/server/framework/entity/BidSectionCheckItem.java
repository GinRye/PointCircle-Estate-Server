package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_bid_section_check_item")
public class BidSectionCheckItem  extends JpaBaseEntity<String> {
	
	@Id 
	@Column(name = "bid_section_check_item_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "bid_section_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_problem_class_bid_section_id"))
	private BidSection bidSection;
	
	@Column(name = "bid_section_id", updatable = false, insertable = false)
	private String bidSectionId;
	
	@ManyToOne
	@JoinColumn(name = "check_item_id", nullable = false,
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_problem_class_problem_class_id"))
	private ProblemClass problemclass;
	
	@Column(name = "check_item_id", updatable = false, insertable = false)
	private String checkItemId;
	
	@ManyToOne
	@JoinColumn(name = "contractor_id",
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_problem_class_contractor_id"))
	private Contractor contractor;
	
	@Column(name = "contractor_id", updatable = false, insertable = false)
	private String contractorId;
	
	@ManyToOne
	@JoinColumn(name = "repair",
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_problem_class_repair_id"))
	private User repair;
	
	@Column(name = "repair", updatable = false, insertable = false)
	private String repairId;
	
	@Column(name = "reinspect", length = 6000)
	private String reinspect;
	
	@Column(name = "cc", length = 6000)
	private String cc;
	
	@Column(name = "check_item_type" )
	private String checkItemType;
}