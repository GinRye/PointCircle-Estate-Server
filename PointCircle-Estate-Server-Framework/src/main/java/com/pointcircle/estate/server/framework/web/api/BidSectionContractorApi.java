package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.BidSectionContractor;
import com.pointcircle.estate.server.framework.repository.BidSectionContractorRepository;

@RestController
@RequestMapping("/api/bidSectionContractor")
public class BidSectionContractorApi implements
	IFindAllRest<BidSectionContractor, String, BidSectionContractorApi.BidSectionContractorEntityVo> {

	@Autowired
	private BidSectionContractorRepository bidSectionContractorRepository;
	
	public class BidSectionContractorEntityVo extends EntityVo<BidSectionContractor, String> {
		public BidSectionContractorEntityVo(BidSectionContractor entity) {
			super(entity);
		}
		@JsonProperty("bidSectionId")
		public String getBidSectionId() {
			return entity.getBidSectionId();
		}
		@JsonProperty("contractorId")
		public String getContractorId() {
			return entity.getContractorId();
		}
	}

	@Override
	public IRepository<BidSectionContractor, String> getRepository() {
		return bidSectionContractorRepository;
	}

	
}