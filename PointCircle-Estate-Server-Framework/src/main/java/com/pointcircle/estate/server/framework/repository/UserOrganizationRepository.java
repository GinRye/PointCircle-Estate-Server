package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.Organization;
import com.pointcircle.estate.server.framework.entity.UserOrganization;

@Repository("userOrganizationRepository")
public interface UserOrganizationRepository extends IRepository<UserOrganization, String> {
	
	List<UserOrganization> findByUser(User user);
	
	List<UserOrganization> findByOrganization(Organization organization);

	@Query("select o.user from UserOrganization o where o.organization = :organization")
	List<User> findUserByOrganization(@Param("organization") Organization organization);

	@Query("select o.organization from UserOrganization o where o.user = :user")
	List<Organization> findOrganizationByUser(@Param("user") User user);
	
	UserOrganization findByUserAndOrganization(User user, Organization organization);
	
	@Query("select o from UserOrganization o where o.user = :user and o.organization in :organizations")
	List<UserOrganization> findByUserAndOrganizationIn(@Param("user") User user, @Param("organizations") List<Organization> organizations);
}
