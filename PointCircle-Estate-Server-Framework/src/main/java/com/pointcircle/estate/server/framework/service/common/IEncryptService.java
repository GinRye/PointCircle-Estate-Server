package com.pointcircle.estate.server.framework.service.common;

public interface IEncryptService {
	String encrypt(String password);
}
