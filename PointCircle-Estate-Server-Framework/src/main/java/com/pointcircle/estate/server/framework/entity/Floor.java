package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_floor")
public class Floor extends JpaBaseEntity<String> {

	@Id
	@Column(name = "floor_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "floor_name", nullable = false)
	private String floorName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "building_id", nullable = false, 
		foreignKey = @ForeignKey(name = "tb_floor_building_id"))
	private Building building;
	
	@Column(name = "building_id", updatable = false, insertable = false)
	private String buildingId;
	
	@Column(name = "is_public_area")
	private boolean isPublicArea = false;
	
	@Column(name = "is_top")
	private boolean isTop = false;
	
	@Column(name = "sort_number", nullable = false)
	private int sortNumber;
}
