package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.entity.Building;
import com.pointcircle.estate.server.framework.entity.Project;

@Repository
public interface BuildingRepository extends IRepository<Building, String> {
	
	List<Building> findByProject(Project project);

	@Query("select o from Building o where o.bidSection = :bidSection order by buildingName asc")
	List<Building> findByBidSection(@Param("bidSection") BidSection bidSection);
	
	Building findByProjectAndBuildingName(
		@Param("project") Project project, 
		@Param("buildingName") String buildingName);
}
