package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.entity.BidSectionCheckItem;
import com.pointcircle.estate.server.framework.entity.ProblemClass;

@Repository("bidSectionCheckItemRepository")
public interface BidSectionCheckItemRepository extends IRepository<BidSectionCheckItem,String>{
	
	@Query("select o.problemclass from BidSectionCheckItem o where o.bidSection = :bidSection")
	List<ProblemClass> findCheckItemByBidSection(@Param("bidSection") BidSection bidSection);
	
	@Query("select o from BidSectionCheckItem o where o.bidSection = :bidSection")
	List<BidSectionCheckItem> findByBidSection(@Param("bidSection") BidSection bidSection);
	
	@Query("select o from BidSectionCheckItem o where o.bidSection = :bidSection and o.problemclass = :problemclass")
	BidSectionCheckItem findByBidSectionAndCheckItem(@Param("bidSection") BidSection bidSection,@Param("problemclass") ProblemClass problemclass);
}
