package com.pointcircle.estate.server.framework.repository;

import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.User;
import com.pointcircle.estate.server.framework.entity.UserInfo;

@Repository
public interface UserInfoRepository extends IRepository<UserInfo, String> {
	UserInfo findByUser(User user);
}
