package com.pointcircle.estate.server.framework.web.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.ProblemClass;
import com.pointcircle.estate.server.framework.repository.ProblemClassRepository;

@RestController
@RequestMapping("/api/problemClass")
public class ProblemClassApi implements IFindAllRest<ProblemClass, String, ProblemClassApi.ProblemClassEntityVo> {
	
	@Autowired
	private ProblemClassRepository problemClassRepository;
	
	@Override
	public IRepository<ProblemClass, String> getRepository() {
		return problemClassRepository;
	}

	public class ProblemClassEntityVo extends EntityVo<ProblemClass, String> {
		public ProblemClassEntityVo(ProblemClass entity) {
			super(entity);
		}
		@JsonProperty("problemClassName")
		public String getProblemClassName() {
			return entity.getProblemClassName();
		}
		@JsonProperty("level")
		public Integer getLevel() {
			return entity.getLevel();
		}
		@JsonProperty("type")
		public String getType() {
			return entity.getType();
		}
		@JsonProperty("childProblemClassIds")
		public List<String> getChildProblemClass() {
			List<String> childProblemClassIds = problemClassRepository
				.findProblemClassByParentProblemClass(entity)
				.stream().map((problemClass) -> {
					return problemClass.getId();
				}).collect(Collectors.toList());
			return childProblemClassIds;
		}
		@JsonProperty("parentProblemClassId")
		public String getParentProblemClassId() {
			return entity.getParentProblemClassId();
		}
		@JsonProperty("code")
		public String code() {
			return entity.getCode();
		}
	}
}