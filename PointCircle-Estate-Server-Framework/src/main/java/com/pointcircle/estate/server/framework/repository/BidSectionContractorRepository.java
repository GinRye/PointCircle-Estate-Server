package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.entity.BidSectionContractor;
import com.pointcircle.estate.server.framework.entity.Contractor;

@Repository("bidSectionContractorRepository")
public interface BidSectionContractorRepository extends IRepository<BidSectionContractor,String>{
	
	@Query("select o.contractor from BidSectionContractor o where o.bidSection = :bidSection")
	List<Contractor> findContractorByBidSection(@Param("bidSection") BidSection bidSection);
	
	@Query("select o from BidSectionContractor o where o.bidSection = :bidSection")
	List<BidSectionContractor> findByBidSection(@Param("bidSection") BidSection bidSection);
	
	@Query("select o from BidSectionContractor o where o.bidSection = :bidSection and o.contractor = :contractor")
	BidSectionContractor findByBidSectionAndContractor(@Param("bidSection") BidSection bidSection,@Param("contractor") Contractor contractor);
	
}
