package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_checkItem_guideline")
public class CheckItemGuideline extends JpaBaseEntity<String> {
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	//检查指引
	@Column(name = "content", length = 4096)
	private String content;
	
	//合格率计算方法		1标准,2极差,3高宽,4开间进深,5方正性,6观感,7标准+设计值    默认为1
	@Column(name = "passrate_mode")
	private String passrateMode;
	
	//检查项合格标准最大值
	@Column(name = "standard_max")
	private Double standardMax;
	
	//检查项合格标准最小值
	@Column(name = "standard_min")
	private Double standardMin;
	
	//测区数   默认为3
	@Column(name = "measurement_area_num")
	private Integer measurementAreaNum;
	
	//测区点数   默认为5
	@Column(name = "per_area_points")
	private Integer perAreaPoints;
	
	//部位划分		1分户,2不分单元(整层),3整栋,4桩基,5分单元(整层)      默认为1
	@Column(name = "process_mode")
	private String processMode;
	
	@ManyToOne
	@JoinColumn(name = "item_id",
		foreignKey = @ForeignKey(name = "fk_tb_problem_class_item_id"))
	private ProblemClass problemClass;
	
	@Column(name = "item_id", updatable = false, insertable = false)
	private String itemId;
}
