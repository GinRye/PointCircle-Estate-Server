package com.pointcircle.estate.server.framework.web.api;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindByIdRest;
import com.pointcircle.core.web.RestResponseList;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;
import com.pointcircle.estate.server.framework.entity.CheckQualityProblem;
import com.pointcircle.estate.server.framework.entity.CheckQualityReinspect;
import com.pointcircle.estate.server.framework.entity.Ids;
import com.pointcircle.estate.server.framework.repository.CheckQualityBatchRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityProblemRepository;
import com.pointcircle.estate.server.framework.repository.CheckQualityReinspectRepository;

@RestController
@RequestMapping("/api/checkQualityReinspect")
public class CheckQualityReinspectApi implements 
	IFindByIdRest<CheckQualityReinspect, String, CheckQualityReinspectApi.CheckQualityReinspectEntityVo> {

	@Autowired
	private CheckQualityReinspectRepository repository;
	
	@Autowired
	private CheckQualityProblemRepository checkQualityProblemRepository;
	
	@Autowired
	private CheckQualityBatchRepository checkQualityBatchRepository;
	
	public class CheckQualityReinspectEntityVo extends EntityVo<CheckQualityReinspect, String> {

		public CheckQualityReinspectEntityVo(CheckQualityReinspect entity) {
			super(entity);
		}
		@JsonProperty("checkqualityProblemId")
		public String getCheckqualityProblemId() {
			return entity.getCheckqualityProblemId();
		}
		@JsonProperty("reinspect")
		public String reinspect() {
			return entity.getReinspectId();
		}
	}

	@Override
	public IRepository<CheckQualityReinspect, String> getRepository() {
		return repository;
	}
	
	@GetMapping("findByBatchId")
	public RestResponseList findByBatchId(
			@RequestParam("batchId") String batchId){
		CheckQualityBatch batch = checkQualityBatchRepository.findOne(batchId);
		List<CheckQualityProblem> problems = checkQualityProblemRepository.findByCheckQualityBatch(batch);
		List<CheckQualityReinspect> CheckQualityReinspects = problems.size() > 0 ? 
				repository.findByCheckQualityProblemIn(problems) : new LinkedList<CheckQualityReinspect>();
		return this.createRestResponseList(CheckQualityReinspects);
	}
	
	@GetMapping("findByProblemIds")
	public RestResponseList findByProblemIds(@RequestParam("problemIds") List<String> problemIds){
		if(problemIds.size() < 1) {
			return this.createRestResponseList(new LinkedList<CheckQualityReinspect>());
		}
		List<CheckQualityProblem> problems = checkQualityProblemRepository.findProblemsByProblemIds(problemIds);
		List<CheckQualityReinspect> CheckQualityReinspects = problems.size() > 0 ? 
				repository.findByCheckQualityProblemIn(problems) : new LinkedList<CheckQualityReinspect>();
		return this.createRestResponseList(CheckQualityReinspects);
	}
	
	@GetMapping("findByCheckQualityProblemId")
	public RestResponseList findByCheckQualityProblemId(
		@RequestParam("checkQualityProblemId") String CheckQualityProblemId) {
		CheckQualityProblem checkQualityProblem = checkQualityProblemRepository.findOne(CheckQualityProblemId);
		List<CheckQualityReinspect> CheckQualityReinspects = repository.findByCheckQualityProblem(checkQualityProblem);
		return this.createRestResponseList(CheckQualityReinspects);
	}
}
