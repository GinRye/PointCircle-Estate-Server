package com.pointcircle.estate.server.framework.entity;

public enum ProblemStatus {
	待整改,
	待复验,
	已通过,
	已关闭,
	检查中,
	已整改,
	检查完毕
}
