package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.Role;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_role_association_role")
public class RoleAssociationRole extends JpaBaseEntity<String> {
	
	@Id 
	@Column(name = "role_association_role_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "role_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_role_association_role_role_id"))
	private Role role;
	
	@ManyToOne
	@JoinColumn(name = "external_role_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_role_association_role_external_role_id"))
	private Role externalRole;
}
