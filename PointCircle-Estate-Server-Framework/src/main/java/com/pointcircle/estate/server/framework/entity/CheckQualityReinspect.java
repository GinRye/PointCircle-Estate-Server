package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_checkquality_reinspect")
public class CheckQualityReinspect extends JpaBaseEntity<String> {

	@Id
	@Column(name = "checkquality_reinspect_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "checkquality_problem_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_reinspect_checkquality_problem_id"))
	private CheckQualityProblem checkQualityProblem;
	
	@Column(name = "checkquality_problem_id", updatable = false, insertable = false)
	private String checkqualityProblemId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reinspect", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_reinspect_reinspect"))
	private User reinspect;
	
	@Column(name = "reinspect", updatable = false, insertable = false)
	private String reinspectId;
}
