package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.House;
import com.pointcircle.estate.server.framework.repository.HouseRepository;

@RestController
@RequestMapping("/api/house")
public class HouseApi implements 
	IFindAllRest<House, String, HouseApi.HouseEntityVo> {
	
	@Autowired
	private HouseRepository houseRepository;
	
	public class HouseEntityVo extends EntityVo<House, String> {

		public HouseEntityVo(House entity) {
			super(entity);
		}
		@JsonProperty("houseName")
		public String getHouseName() {
			return entity.getHouseName();
		}
		@JsonProperty("houseCompleteName")
		public String getHouseCompleteName() {
			return entity.getHouseCompleteName();
		}
		@JsonProperty("houseTypeId")
		public String getHouseTypeId() {
			return entity.getHouseTypeId();
		}
		@JsonProperty("houseNumber")
		public String getHouseNumberId() {
			return entity.getHouseNumber();
		}
		@JsonProperty("floorId")
		public String getFloorId() {
			return entity.getFloorId();
		}
		@JsonProperty("unitId")
		public String getUnitId() {
			return entity.getUnitId();
		}
		@JsonProperty("isPublicArea")
		public boolean getIsPublicArea() {
			return entity.isPublicArea();
		}
	}
	
	@Override
	public IRepository<House, String> getRepository() {
		return houseRepository;
	}
}
