package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_batch_cc")
public class BatchCc extends JpaBaseEntity<String> {

	@Id
	@Column(name = "batch_cc_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "batch_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_batch_cc_batch_id"))
	private CheckQualityBatch checkQualityBatch;
	
	@Column(name = "batch_id", updatable = false, insertable = false)
	private String batchId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_batch_cc_user_id"))
	private User user;
	
	@Column(name = "user_id", updatable = false, insertable = false)
	private String userId;
}
