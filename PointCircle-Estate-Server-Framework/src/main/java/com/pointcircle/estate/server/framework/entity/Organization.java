package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_organization")
public class Organization extends JpaBaseEntity<String> {

	@Id
	@Column(name = "organization_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "organization_name", nullable = false)
	private String organizationName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_organization_id",
		foreignKey = @ForeignKey(name = "fk_tb_organization_parent_organization_id"))
	private Organization parentOrganization;
	
	@Column(name = "is_head_company")
	private boolean isHeadCompany = false;
	
	@Column(name = "is_branch_company")
	private boolean isBranchCompany = false;
}
