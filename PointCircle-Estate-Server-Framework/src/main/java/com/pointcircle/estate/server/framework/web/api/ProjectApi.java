package com.pointcircle.estate.server.framework.web.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Project;
import com.pointcircle.estate.server.framework.repository.ProjectRepository;

@RestController
@RequestMapping("/api/project")
public class ProjectApi implements IFindAllRest<Project, String, ProjectApi.ProjectEntityVo> {
	
	@Autowired
	private ProjectRepository projectRepository;
	
	public class ProjectEntityVo extends EntityVo<Project, String> {
		public ProjectEntityVo(Project entity) {
			super(entity);
		}
		@JsonProperty("projectName")
		public String getProjectName() {
			return entity.getProjectName();
		}
		@JsonProperty("organizationId")
		public String getOrganizationId() {
			return entity.getOrganizationId();
		}
		@JsonProperty("parentProjectId")
		public String getParentProjectId() {
			return entity.getParentProjectId();
		}
		@JsonProperty("childProjectIds")
		public List<String> getChildProjectIds() {
			List<Project> childProjects = projectRepository.findByParentProject(entity);
			return childProjects.stream()
				.map(x -> x.getId())
				.collect(Collectors.toList());
		}
	}

	@Override
	public IRepository<Project, String> getRepository() {
		return projectRepository;
	}
}
