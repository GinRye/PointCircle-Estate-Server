package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.entity.Project;

@Repository("bidSectionRepository")
public interface BidSectionRepository extends IRepository<BidSection,String> {
	
	List<BidSection> findByProject(Project project);
	
	@Query("select o from BidSection o order by o.bidSectionName asc")
	List<BidSection> findAll();
	
	@Query("select o from BidSection o where o.project in :projects order by o.bidSectionName asc")
	Page<BidSection> findByProjectIn(@Param("projects") List<Project> projects, Pageable pageable);
	
}