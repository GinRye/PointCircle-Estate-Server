package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_unit")
public class Unit extends JpaBaseEntity<String> {

	@Id
	@Column(name = "unit_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "unit_name", nullable = false)
	private String unitName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "building_id", nullable = false, 
		foreignKey = @ForeignKey(name = "tb_unit_building_id"))
	private Building building;
	
	@Column(name = "building_id", updatable = false, insertable = false)
	private String buildingId;
	
	@Column(name = "house_number_list", length = 1024)
	private String houseNumberList;
	
	@Column(name = "door_amount")
	private int doorAmount;
	
	@Column(name = "sort_number", nullable = false)
	private int sortNumber;
}
