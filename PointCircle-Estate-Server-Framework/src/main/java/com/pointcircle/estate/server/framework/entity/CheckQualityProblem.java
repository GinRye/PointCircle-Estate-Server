package com.pointcircle.estate.server.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_checkquality_problem",
	indexes = {
		@Index(name = "idx_tb_checkquality_problem_indexTimeout", columnList = "indexTimeout"),
		@Index(name = "index_tb_checkquality_problem_regist_date", columnList = "regist_date")
	}
)
public class CheckQualityProblem extends JpaBaseEntity<String> {

	@Id
	@Column(name = "checkquality_problem_id", nullable = false)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "batch_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_batch_id"))
	private CheckQualityBatch checkQualityBatch;
	
	@Column(name = "batch_id", updatable = false, insertable = false)
	private String batchId;
	
	@Column(name = "batch_type")
	@Enumerated(EnumType.STRING)
	private BatchType batchType = BatchType.安全文明;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "item_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_item_id"))
	private ProblemClass problemClass;
	
	@Column(name = "item_id", updatable = false, insertable = false)
	private String itemId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contractor_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_contractor_id"))
	private Contractor contractor;
	
	@Column(name = "contractor_id", updatable = false, insertable = false)
	private String contractorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "desc_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_desc_id"))
	private Problem problem;
	
	@Column(name = "desc_id", updatable = false, insertable = false)
	private String descId;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "image_file", length = 2048)
	private String imageFile;
	
	@Column(name = "small_image_file", length = 1024)
	private String smallImageFile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "building_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_building_id"))
	private Building building;

	@Column(name = "building_id", updatable = false, insertable = false)
	private String buildingId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_unit_id"))
	private Unit unit;

	@Column(name = "unit_id", updatable = false, insertable = false)
	private String unitId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "floor_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_floor_id"))
	private Floor floor;
	
	@Column(name = "floor_id", updatable = false, insertable = false)
	private String floorId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "room_id",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_room_id"))
	private House house;
	
	@Column(name = "room_id", updatable = false, insertable = false)
	private String roomId;
	
	@Column(name = "place")
	private String place;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private ProblemStatus status;
	
	@Column(name = "emergency_degree")
	private String emergencyDegree;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "regist_by", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_regist_by"))
	private User registBy;
	
	@Column(name = "regist_by", updatable = false, insertable = false)
	private String registByUserId;
	
	@Column(name = "regist_date")
	private Date registDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "repair_by",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_repair_by"))
	private User repairBy;
	
	@Column(name = "repair_by", updatable = false, insertable = false)
	private String repairByUserId;
	
	@Column(name = "repair_deadline")
	private Date repairDeadline;
	
	@Column(name = "repair_date")
	private Date repairDate;
	
	@Column(name = "repair_remark")
	private String repairRemark;
	
	@Column(name = "repair_image_file", length = 2048)
	private String repairImageFile;
	
	@Column(name = "small_repair_image_file", length = 1024)
	private String smallRepairImageFile;
	
	@Column(name = "problem_values", length = 1024)
	private String problemValues;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sent_back_by", 
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_send_back_by"))
	private User sendBackBy;
	
	@Column(name = "send_back_by", updatable = false, insertable = false)
	private String sendBackByUserId;
	
	@Column(name = "sent_back_date")
	private Date sentBackDate;
	
	@Column(name = "sent_back_times")
	private Integer sentBackTimes;
	
	@Column(name = "sent_back_remark")
	private String sentBackRemark;
	
	@Column(name = "sent_back_image_file", length = 1024)
	private String sentBackImageFile;
	
	@Column(name = "small_sent_back_image_file", length = 1024)
	private String smallSentBackImageFile;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reinspect_by",
		foreignKey = @ForeignKey(name = "fk_tb_checkquality_problem_reinspect_by"))
	private User reinspectBy;
	
	@Column(name = "reinspect_by", updatable = false, insertable = false)
	private String reinspectByUserId;
	
	@Column(name = "reinspect_image_file", length = 1024)
	private String reinspectImageFile;
	
	@Column(name = "small_reinspect_image_file", length = 1024)
	private String smallReinspectImageFile;

	@Column(name = "reinspect_remark")
	private String reinspectRemark;
	
	@Column(name = "reinspect_completed_date")
	private Date reinspectCompletedDate;
	
	@Column(name = "close_date")
	private Date closeDate;
	
	@Column(name = "close_reason")
	private String closeReason;
	
	@Column(name = "drawing_file", length = 1024)
	private String drawingFile;
	
	@Column(name = "indexTimeout")
	private String indexTimeout;
}
