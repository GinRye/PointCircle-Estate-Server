package com.pointcircle.estate.server.mobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.pointcircle.core", "com.pointcircle.estate.server.framework", "com.pointcircle.estate.server.mobile"})
public class EstateServerMobileApplication {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(EstateServerMobileApplication.class, args);
	}
}
