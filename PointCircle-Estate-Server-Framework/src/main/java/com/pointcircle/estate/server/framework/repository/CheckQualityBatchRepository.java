package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.BatchType;
import com.pointcircle.estate.server.framework.entity.BidSection;
import com.pointcircle.estate.server.framework.entity.CheckQualityBatch;

@Repository
public interface CheckQualityBatchRepository extends IRepository<CheckQualityBatch, String> {

	List<CheckQualityBatch> findByBidSectionAndBatchType(BidSection bidSection, BatchType batchType);
	
	List<CheckQualityBatch> findByBidSection(BidSection bidSection);
	
	@Query("select o from CheckQualityBatch o")
	List<CheckQualityBatch> findAll();
}
