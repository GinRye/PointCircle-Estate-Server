package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_bid_section_contractor")
public class BidSectionContractor extends JpaBaseEntity<String> {
	
	@Id 
	@Column(name = "bid_section_check_item_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "bid_section_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_contractor_bid_section_id"))
	private BidSection bidSection;
	
	@Column(name = "bid_section_id", updatable = false, insertable = false)
	private String bidSectionId;
	
	@ManyToOne
	@JoinColumn(name = "contractor_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_bid_section_contractor_contractor_id"))
	private Contractor contractor;
	
	@Column(name = "contractor_id", updatable = false, insertable = false)
	private String contractorId;
}
