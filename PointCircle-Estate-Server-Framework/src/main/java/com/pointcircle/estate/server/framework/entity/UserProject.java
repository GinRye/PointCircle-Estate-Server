package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;
import com.pointcircle.core.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_user_project")
public class UserProject extends JpaBaseEntity<String> {
	
	@Id
	@Column(name = "user_project_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_user_project_user_id"))
	private User user;
	
	@Column(name = "user_id", updatable = false, insertable = false)
	private String userId;

	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_user_project_project_id"))
	private Project project;
	
	@Column(name = "project_id", updatable = false, insertable = false)
	private String projectId;
}
