package com.pointcircle.estate.server.framework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.core.entity.Role;
import com.pointcircle.core.entity.RolePermission;

@Repository("rolePermissionRepository")
public interface RolePermissionRepository extends IRepository<RolePermission, String> {
	
	List<RolePermission> findByRole(Role role);

	@Query("select o.permission from RolePermission o where o.role = :role")
	List<String> findPermissionByRole(@Param("role") Role role);
	
	@Query("select o from RolePermission o where o.role = :role and o.permission = :permission")
	RolePermission findRolePermissionByRoleAndPermission(@Param("role") Role role, @Param("permission") String permission);

	@Query("select o.role from RolePermission o where o.permission = :permission")
	List<Role> findRoleByPermission(@Param("permission") String permission);
}
