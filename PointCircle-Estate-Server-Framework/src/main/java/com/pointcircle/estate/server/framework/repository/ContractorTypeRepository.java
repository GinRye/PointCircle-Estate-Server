package com.pointcircle.estate.server.framework.repository;

import org.springframework.stereotype.Repository;

import com.pointcircle.core.IRepository;
import com.pointcircle.estate.server.framework.entity.ContractorType;

@Repository("contractorTypeRepository")
public interface ContractorTypeRepository extends IRepository<ContractorType, String>{
	
	ContractorType findByContractorTypeName(String contractorTypeName);
}
