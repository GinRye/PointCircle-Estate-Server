package com.pointcircle.estate.server.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pointcircle.core.JpaBaseEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_building")
public class Building extends JpaBaseEntity<String> {

	@Id 
	@Column(name = "building_id")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	private String id;
	
	@Column(name = "building_name", nullable = false)
	private String buildingName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id", nullable = false,
		foreignKey = @ForeignKey(name = "fk_tb_building_project_id"))
	private Project project;
	
	@Column(name = "project_id", updatable = false, insertable = false)
	private String projectId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bid_section_id", 
		foreignKey = @ForeignKey(name = "fk_tb_building_bid_section_id"))
	private BidSection bidSection;
	
	@Column(name = "bid_section_id", updatable = false, insertable = false)
	private String bidSectionId;
	
	@Column(name = "sort_number", nullable = false)
	private int sortNumber;
	
	@Column(name = "model_id", nullable = false)
	private String modelId;
}
