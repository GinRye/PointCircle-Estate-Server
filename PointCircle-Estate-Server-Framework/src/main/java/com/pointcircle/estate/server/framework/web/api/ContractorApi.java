package com.pointcircle.estate.server.framework.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pointcircle.core.IRepository;
import com.pointcircle.core.web.EntityVo;
import com.pointcircle.core.web.IFindAllRest;
import com.pointcircle.estate.server.framework.entity.Contractor;
import com.pointcircle.estate.server.framework.repository.ContractorRepository;

@RestController
@RequestMapping("/api/contractor")
public class ContractorApi implements IFindAllRest<Contractor, String, ContractorApi.ContractorEntityVo> {

	@Autowired
	private ContractorRepository contractorRepository;
	
	@Override
	public IRepository<Contractor, String> getRepository() {
		return contractorRepository;
	}
	
	public class ContractorEntityVo extends EntityVo<Contractor, String> {
		public ContractorEntityVo(Contractor entity) {
			super(entity);
		}
		@JsonProperty("contractorName")
		public String getContractorName() {
			return entity.getContractorName();
		}
		@JsonProperty("remark")
		public String getRemark() {
			return entity.getRemark();
		}
		@JsonProperty("contractorShortName")
		public String getContractorShortName() {
			return entity.getContractorShortName();
		}
		@JsonProperty("contractorTypeId")
		public String getContractorTypeId() {
			return entity.getContractorTypeId();
		}
		@JsonProperty("manager")
		public String getManager() {
			return entity.getManager();
		}
		@JsonProperty("contacter")  
		public String getContacter() {
			return entity.getContacter();
		}
		@JsonProperty("contactPhone")
		public String getContactPhone() {
			return entity.getContactPhone();
		}
		@JsonProperty("userAmount")
		public Integer getUserAmount() {
			return entity.getUserAmount();
		}
		@JsonProperty("socialCreditCode")
		public String getSocialCreditCode() {
			return entity.getSocialCreditCode();
		}
		@JsonProperty("registrationNumber")
		public String getRegistrationNumber() {
			return entity.getRegistrationNumber();
		}
	}
}
